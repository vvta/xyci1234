package xyci1234MV;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

// daca este timp le mai poti arata teste parametrizate
//https://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-tests

//mai jos am pus teste pentru assertAll, Disabled, timeout, aruncarea de exceptie

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CarteTest {

    Carte c1, c2, c3;



    @BeforeEach
    public void setUp() {
        c1=new Carte();
        c1.setTitlu("Povesti");
        c1.setAnAparitie("2000");
        c1.adaugaAutor("Ion Creanga");

        c2= new Carte();
        System.out.println("Before test");

        c3= null;
    }

    @AfterEach
    public void tearDown() throws Exception {
        c1=null;

        System.out.println("After test");
    }


    @Test
    public void testConstructor1(){
        assertNotEquals(c2, null );
        assertEquals(c3, null);
        assertNull(c3 );
    }

    //metoda assertAll
    //testare atributelor unui obiect
    @Test
    public void test_Carte_attributes(){
        assertAll("Carte",
                () -> assertEquals("Povesti", c1.getTitlu()),
                () -> assertEquals("Ion Creanga", c1.getAutori().get(0)),
                () -> assertEquals("2000", c1.getAnAparitie())
        );
    }


    @Disabled //echivalent cu ignore
    @Test
    public void getTitlu() {
        assertEquals("titlu = Povesti", "Povesti", c1.getTitlu());
    }

    //echivalent cu @Ignore
    @Disabled("setTitlu - in progress")
    @Test
    public void setTitlu() {
        c1.setTitlu("Povesti II");
        assertEquals("titlu = Povesti II", "Povesti II", c1.getTitlu());
    }


    //timeout: varianta cu adnotarea Timeout
    @Test
    //@Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
    @Timeout(value = 2, unit = TimeUnit.SECONDS)
    //@Timeout(5)
    public void cautaDupaAutor() {
        try {
            //Thread.sleep(100);
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals( c1.cautaDupaAutor("Ion Creanga"), true);
    }

    //timeout: varianta cu metoda asserTimeout
    @Test
    public void cautaDupaAutor_cu_assertTimeout() {
        Assertions.assertTimeout(Duration.ofSeconds(4), ()->{delay_and_call();}, String.valueOf(true));
    }

    private void delay_and_call(){
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();};
        c1.cautaDupaAutor("Ion Creanga");
    }


    //aruncarea unei exceptii- metoda assertThrows
    @Order(1) //se ruleaza in functie de ordinea precizata, se pune adnotarea la inceputul clasei
    @Test
    public void testConstructor2(){
        //assertEquals(c3, null);
        //throw new NullPointerException();
        assertThrows(NullPointerException.class, ()->{c3.getTitlu();});
    }

    @BeforeAll
    public static void setup(){
        System.out.println("Before class");
    }



    @AfterAll
    public static void teardown(){
        System.out.println("After class");
    }


}